﻿namespace CurveTracer
{
    partial class SerialDataDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbxSerialLog = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // tbxSerialLog
            // 
            this.tbxSerialLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxSerialLog.Location = new System.Drawing.Point(0, 0);
            this.tbxSerialLog.Multiline = true;
            this.tbxSerialLog.Name = "tbxSerialLog";
            this.tbxSerialLog.ReadOnly = true;
            this.tbxSerialLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbxSerialLog.Size = new System.Drawing.Size(800, 450);
            this.tbxSerialLog.TabIndex = 1;
            this.tbxSerialLog.TextChanged += new System.EventHandler(this.tbxSerialLog_TextChanged);
            // 
            // SerialDataDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tbxSerialLog);
            this.Name = "SerialDataDialog";
            this.Text = "Serial Log";
            this.Shown += new System.EventHandler(this.SerialDataDialog_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbxSerialLog;
    }
}