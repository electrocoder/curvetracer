/*
 * lvSSD1963.h
 *
 *  Created on: 2018. dec. 27.
 *      Author: zoli
 *
 *  LittlevGL wrapper for the SSD1963 driver
 */

#ifndef HWDRV_INC_LVSSD1963_H_
#define HWDRV_INC_LVSSD1963_H_

#include "lvgl.h"
#include "lv_conf.h"

void SSD1963_Flush(int32_t x1, int32_t y1, int32_t x2, int32_t y2, const lv_color_t * color_p);
void SSD1963_Map(int32_t x1, int32_t y1, int32_t x2, int32_t y2, const lv_color_t * color_p);
void SSD1963_Fill(int32_t x1, int32_t y1, int32_t x2, int32_t y2, lv_color_t color);

#endif /* HWDRV_INC_LVSSD1963_H_ */
