/*
 * MCP45HVx1.h
 *
 *  Created on: 2018. aug. 26.
 *      Author: zoli
 */

#ifndef MCP45HVX1_H_
#define MCP45HVX1_H_

void MCP45HVx1_SetWiper(uint16_t addr, uint8_t data);

#endif /* MCP45HVX1_H_ */
