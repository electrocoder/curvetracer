﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurveTracer
{
    /// <summary>
    /// Represent the response sent by the Curve Tracer device
    /// </summary>
    public class CTResponse
    {
        /// <summary>
        /// Response code determine the type of data sent
        /// </summary>
        public string Code;
        /// <summary>
        /// The parameter array of the response
        /// </summary>
        public string[] Params;
        /// <summary>
        /// The binary data if the response contains such
        /// </summary>
        public byte[] BinData;
        public CTResponse() { }
        public CTResponse(byte[] data)
        {
            DeserializeCOM(data);
        }
        /// <summary>
        /// Return the response in original format without the included binary data
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Code + ',' + string.Join(",", Params);
        }

        public void DeserializeCOM(byte[] data)
        {
            string currentline = "";    // Placeholder of the nonbinary data
            for (int i = 0; i < data.Length; i++)
            {
                if ((char)data[i] != '!')
                {
                    currentline += (char)data[i];
                }
                else
                {
                    this.BinData = new byte[data.Length - i -1];
                    Array.ConstrainedCopy(data, i + 1 , BinData, 0, BinData.Length);
                    break;
                }
            }
            this.Code = currentline.Substring(0, currentline.IndexOf(','));
            currentline = currentline.TrimEnd(',');
            this.Params = currentline.Substring(currentline.IndexOf(',') + 1, currentline.Length  - currentline.IndexOf(',') - 1).Split(',');

        }
    }
}
