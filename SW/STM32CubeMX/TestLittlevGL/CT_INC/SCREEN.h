/*
 * SCREEN.h
 *
 *  Created on: 2018. nov. 27.
 *      Author: zoli
 */

#ifndef SCREEN_H_
#define SCREEN_H_

void SCREEN_Init();
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim);
void SCREEN_hello();
void HAL_SYSTICK_Callback(void);

#endif /* SCREEN_H_ */
