﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurveTracer
{
    public enum CommandCode
    {
        WFG_SETPERIOD = 0,
        WFG_SELFREQ = 1,
        WFG_SETFREQ	= 2,
        WFG_GETFREQ = 3,
        WFG_GENSINTBL =	4,
        WFG_GENCALTBL =	5,

        WFG_MODE = 6,
        WFG_SETCALSWING	= 7,
        WFG_SETCALPOINT	= 8,
        WFG_GETCALDATA = 9,

        PA_SETWIPER = 10,
        PA_SELVPP =	11,
        PA_SETVPP =	12,
        PA_GETVPP = 13,

        ATT_SELRES = 14,
        ATT_SELDUT = 15,
        ATT_GETRES = 16,

        ATT_GETDUT = 17,
        ATT_SETALTCYCLES = 18,

        MA_SELCURR = 19,
        MA_SELVOLT = 20,
        MA_GETRANGES = 21,

        DAQ_GETDATA = 22,

        USB_CDC_CONNECT	= 23,
        USB_CDC_DISCONNECT = 24,

        NULL = 255
    }
}
