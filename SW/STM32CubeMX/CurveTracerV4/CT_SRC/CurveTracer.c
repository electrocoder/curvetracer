/*
 * CurveTracer.c
 *
 *  Created on: 2018. aug. 19.
 *      Author: zoli
 */


#include <wfg.h>
#include "stm32f4xx_hal.h" // temporary. Just used for testing

#include "CurveTracer.h"
#include "command.h"
#include "usb_fifo.h"
#include "poweramp.h"
#include "measureamp.h"
#include "attenuator.h"
#include "data_acquisition.h"

// #include "lvgl.h"
#include "SCREEN.h"
// #include "../lv_conf.h"
#include <string.h>
// #include "XPT2046.h"

#include "tim.h"

// #include "i2c.h"
// #define I2C_ADDR_MEASUREAMP 0x40
uint8_t data[3] = {0,0,0};


CT_NV_SettingsTypeDef CT_NV_Settings;
CT_V_SettingsTypeDef CT_V_Settings;

// lv_obj_t *tp_interrupt;
// lv_indev_data_t tpdata;
// char buff[80];

void CT_init()
{
	// Screen
 	SCREEN_Init();
	HAL_TIM_Base_Start_IT(&htim7);


//	SCREEN_hello();

	SCREEN_Log("Initialize USB Device\n");
	USB_CDC_Init();
	SCREEN_Log("Load non-volatile data\n");
	// Load settings from non-volatile store
	CT_NV_Load();
	SCREEN_Log("Initialize waveform generator\n");
	WFG_Init();
	SCREEN_Log("Initialize data acquisition\n");
	DAQ_Init();
	SCREEN_Log("Preset frequency\n");
	WFG_SelectFreq(WFG_PRESET_5000Hz);
	SCREEN_Log("Start waveform generator\n");
	WFG_Start();
	SCREEN_Log("Initialize power amplifier\n");
	PA_Init();
	SCREEN_Log("Initialize attenuator\n");
	ATT_Init();
	SCREEN_Log("Initialize measurement amplifier\n");
	MA_Init();
	SCREEN_Log("Initialization complete\n");

//	SCREEN_CreateScope();

// ======= Test code ==============
/*
	HAL_I2C_Master_Transmit(&hi2c1, 0x42, data, 3, 100);
	data[0]=0x12;
	data[1]=0xFF;
	data[2]=0xFF;
	HAL_I2C_Master_Transmit(&hi2c1, 0x42, data, 3, 100);
*/

//	HAL_I2C_Master_Transmit(&hi2c1, I2C_ADDR_MEASUREAMP, data, 2, 100);
//	data[1] = 1;

}

void CT_worker()
{
// The worker is called from the CubeMX generated main.c
	char buff[128];
	if(USB_CDC_RX_numlines > 0)
	{
		USB_CDC_RX_readln((uint8_t*)buff);
		CMD_Call(buff,CMD_COM_DEVICE_USB);
	}
	DAQ_Process_Worker();
// ======= Test code ==============
	/*
	data[0] = 0x0A;
	data[1] = (data[1] == 0x20) ? 1 : data[1] << 1;
	HAL_I2C_Master_Transmit(&hi2c1, I2C_ADDR_MEASUREAMP, data, 2, 100);
	HAL_Delay(500);
	*/

//	HAL_Delay(2000);
//	CMD_Call("1,39");
//	HAL_Delay(2000);
//	CMD_Call("1,30");


//	CDC_Transmit_FS(HiMsg, strlen((char*)HiMsg));
}

void CT_NV_Save()
{

}

void CT_NV_Load()
{
	// Temporary until saving data to flash/eeprom doesn't work
	CT_NV_Settings.wfgCalNullpoint = 2047;
	CT_NV_Settings.wfgCalSwing = 2482;
	CT_NV_Settings.wfgNumSamples = 400;
	CT_NV_Settings.paWiper = 0;
	CT_NV_Settings.attResistanceSet = 0x0;
	CT_NV_Settings.attDUTSel = ATT_DUT_2;
}
