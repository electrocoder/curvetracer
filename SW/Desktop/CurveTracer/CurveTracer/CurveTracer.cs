﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO.Ports;

namespace CurveTracer
{
    public partial class CurveTracer : Form
    {
        SerialCom _com = new SerialCom();
        CTSettings settings = new CTSettings();
        ComDialog dlgCom;
        CalDialog dlgCal;
        SerialDataDialog dlgSerialData;
        // Dictionary<int, string> PortList;
        System.Threading.Timer daq_timer;

        public CurveTracer()
        {
            InitializeComponent();

            _com.BaudRate = 115200;
            _com.Parity = Parity.None;
            _com.DataBits = 8;
            _com.StopBits = StopBits.One;
            _com.Handshake = Handshake.XOnXOff;
            _com.NewLine = "\r";

            

            this.dlgCom = new ComDialog(_com);
            this.dlgCal = new CalDialog(_com);
            this.dlgSerialData = new SerialDataDialog(_com);

            settings.maCurrSel = 0;
            settings.maVoltSel = 1;
            InitFrequency();
            InitPowerAmp();
            InitAttenuator();
            daq_timer = new System.Threading.Timer(this.DAQ_Tick, null, 3000, 500);
        }

        void SerialDataProcessor(CTResponse response)
        {
            switch (response.Code)
            {
                case "a":   // aquired data
                    DAQ_Process(response);
                    // DAQ_AddPoint(data);
                    break;
                case "f":   // Frequency
                    Update_lblFreq(response.Params);
                    break;
                case "m":
                    Update_maButtons(response.Params);
                    break;
                case "v":
                    Update_lblTestVoltage(response.Params);
                    break;
                case "r":
                    Update_lblResistance(response.Params);
                    break;
                case "d":
                    Update_DutButtons(response.Params);
                    break;
                case "c":
                    if (!dlgCal.IsDisposed)
                    {
                        dlgCal.Update_CalData(response.Params);
                    }
                    break;
                default:
                    // just trow away the data, we don't process it
                    break;
            }
        }




        #region Frequency

        private void InitFrequency()
        {
            comboFreq.Items.Add(new PresetElement(20, 0));
            comboFreq.Items.Add(new PresetElement(30, 1));
            comboFreq.Items.Add(new PresetElement(40, 2));
            comboFreq.Items.Add(new PresetElement(50, 3));
            comboFreq.Items.Add(new PresetElement(60, 4));
            comboFreq.Items.Add(new PresetElement(70, 5));
            comboFreq.Items.Add(new PresetElement(80, 6));
            comboFreq.Items.Add(new PresetElement(90, 7));
            comboFreq.Items.Add(new PresetElement(100, 8));
            comboFreq.Items.Add(new PresetElement(110, 9));
            comboFreq.Items.Add(new PresetElement(120, 10));
            comboFreq.Items.Add(new PresetElement(130, 11));
            comboFreq.Items.Add(new PresetElement(140, 12));
            comboFreq.Items.Add(new PresetElement(150, 13));
            comboFreq.Items.Add(new PresetElement(160, 14));
            comboFreq.Items.Add(new PresetElement(170, 15));
            comboFreq.Items.Add(new PresetElement(180, 16));
            comboFreq.Items.Add(new PresetElement(190, 17));
            comboFreq.Items.Add(new PresetElement(200, 18));
            comboFreq.Items.Add(new PresetElement(300, 19));
            comboFreq.Items.Add(new PresetElement(400, 20));
            comboFreq.Items.Add(new PresetElement(500, 21));
            comboFreq.Items.Add(new PresetElement(600, 22));
            comboFreq.Items.Add(new PresetElement(700, 23));
            comboFreq.Items.Add(new PresetElement(800, 24));
            comboFreq.Items.Add(new PresetElement(900, 25));
            comboFreq.Items.Add(new PresetElement(1000, 26));
            comboFreq.Items.Add(new PresetElement(1100, 27));
            comboFreq.Items.Add(new PresetElement(1200, 28));
            comboFreq.Items.Add(new PresetElement(1300, 29));
            comboFreq.Items.Add(new PresetElement(1400, 30));
            comboFreq.Items.Add(new PresetElement(1500, 31));
            comboFreq.Items.Add(new PresetElement(1600, 32));
            comboFreq.Items.Add(new PresetElement(1700, 33));
            comboFreq.Items.Add(new PresetElement(1800, 34));
            comboFreq.Items.Add(new PresetElement(1900, 35));
            comboFreq.Items.Add(new PresetElement(2000, 36));
            comboFreq.Items.Add(new PresetElement(3000, 37));
            comboFreq.Items.Add(new PresetElement(4000, 38));
            comboFreq.Items.Add(new PresetElement(5000, 39));

        }

        public void Update_lblFreq(string[] values)
        {
            if(InvokeRequired)
            {
                this.Invoke(new Action<string[]>(Update_lblFreq), new object[] { values });
                return;
            }
            lblFreq.Text = values[0].PadLeft(8) + " Hz";
        }


        private float freqTextToFloat(string text)
        {
            float freq;
            freq = float.Parse(text);
            return freq;
        }


        private void cbxFreqPreset_CheckedChanged(object sender, EventArgs e)
        {
            float currentFreqSet;
            if(!cbxFreqPreset.Checked)
            {
                currentFreqSet = freqTextToFloat(comboFreq.Text);
            }
            comboFreq.DropDownStyle = (cbxFreqPreset.Checked) ? ComboBoxStyle.DropDownList : ComboBoxStyle.DropDown;
        }


        private void btnFreqUp_Click(object sender, EventArgs e)
        {
            cbxFreqPreset.Checked = true;
        }

        private void btnFreqDown_Click(object sender, EventArgs e)
        {
            cbxFreqPreset.Checked = true;
        }

        private void comboFreq_DropDownStyleChanged(object sender, EventArgs e)
        {
            MessageBox.Show(comboFreq.Text);
        }

        private void comboFreq_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void comboFreq_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_com.IsOpen)
            {
                _com.SendCommand(CommandCode.WFG_SELFREQ, ((PresetElement)comboFreq.SelectedItem).presetIndex.ToString());
                //                _com.SendCommand("1," + ((FreqPresetElement)comboFreq.SelectedItem).presetIndex.ToString());
            }
        }


        #endregion

        #region Power Amplifier

        private void InitPowerAmp()
        {
            comboVolt.Items.Add(new PresetElement((float)0.2, 0));
            comboVolt.Items.Add(new PresetElement((float)0.2, 1));
            comboVolt.Items.Add(new PresetElement((float)0.4, 2));
            comboVolt.Items.Add(new PresetElement((float)0.6, 3));
            comboVolt.Items.Add(new PresetElement((float)0.8, 4));
            comboVolt.Items.Add(new PresetElement((float)1, 5));
            comboVolt.Items.Add(new PresetElement((float)2, 6));
            comboVolt.Items.Add(new PresetElement((float)3, 7));
            comboVolt.Items.Add(new PresetElement((float)4, 8));
            comboVolt.Items.Add(new PresetElement((float)5, 9));
            comboVolt.Items.Add(new PresetElement((float)6, 10));
            comboVolt.Items.Add(new PresetElement((float)7, 11));
            comboVolt.Items.Add(new PresetElement((float)8, 12));
            comboVolt.Items.Add(new PresetElement((float)9, 13));
            comboVolt.Items.Add(new PresetElement((float)10, 14));
            comboVolt.Items.Add(new PresetElement((float)11, 15));
            comboVolt.Items.Add(new PresetElement((float)12, 16));
            comboVolt.Items.Add(new PresetElement((float)13, 17));
            comboVolt.Items.Add(new PresetElement((float)14, 18));
            comboVolt.Items.Add(new PresetElement((float)15, 19));
            comboVolt.Items.Add(new PresetElement((float)16, 20));
            comboVolt.Items.Add(new PresetElement((float)17, 21));
            comboVolt.Items.Add(new PresetElement((float)18, 22));
            comboVolt.Items.Add(new PresetElement((float)19, 23));
            comboVolt.Items.Add(new PresetElement((float)20, 24));

        }

        void Update_lblTestVoltage(string[] values)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<string[]>(Update_lblTestVoltage), new object[] { values });
                return;
            }
            lblTestVoltage.Text = values[0].PadLeft(4) + " V";

        }


        private void comboVolt_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_com.IsOpen)
            {
                _com.SendCommand(CommandCode.PA_SELVPP, ((PresetElement)comboVolt.SelectedItem).presetIndex.ToString());
            }
        }


        #endregion
        #region MeasureAmp

        void Update_maButtons(string[] IVparams)
        {
            MA_I_ClearLeds();
            MA_V_ClearLeds();
            switch(IVparams[0]) // Non-Volatile current
            {
                case "0":   // x0.1
                    btnI01.BackColor = Color.LightGreen;
                    settings.maCurrSel = 0;
                    break;
                case "1":   // x1
                    btnI1.BackColor = Color.LightGreen;
                    settings.maCurrSel = 1;
                    break;
                case "2":   // x10
                    btnI10.BackColor = Color.LightGreen;
                    settings.maCurrSel = 2;
                    break;
                case "255": // Auto mode
                    btnIAuto.BackColor = Color.LightGreen;
                    switch (IVparams[1])
                    {
                        case "0":   // x0.1
                            btnI01.BackColor = Color.LightGreen;
                            settings.maCurrSel = 0x08;
                            break;
                        case "1":   // x1
                            btnI1.BackColor = Color.LightGreen;
                            settings.maCurrSel = 0x09;
                            break;
                        case "2":   // x10
                            btnI10.BackColor = Color.LightGreen;
                            settings.maCurrSel = 0x0A;
                            break;
                    }
                    break;
            }
            switch (IVparams[2]) // Non-Volatile voltage
            {
                case "0":   // x0.1
                    btnV01.BackColor = Color.LightGreen;
                    settings.maVoltSel = 0;
                    break;
                case "1":   // x1
                    btnV1.BackColor = Color.LightGreen;
                    settings.maVoltSel = 1;
                    break;
                case "2":   // x10
                    btnV10.BackColor = Color.LightGreen;
                    settings.maVoltSel = 2;
                    break;
                case "3":   // x100
                    btnV100.BackColor = Color.LightGreen;
                    settings.maVoltSel = 3;
                    break;
                case "4":   // x1000
                    btnV1000.BackColor = Color.LightGreen;
                    settings.maVoltSel = 4;
                    break;
                case "255": // Auto mode
                    btnVAuto.BackColor = Color.LightGreen;
                    switch (IVparams[3])
                    {
                        case "0":   // x0.1
                            btnV01.BackColor = Color.LightGreen;
                            settings.maVoltSel = 0x08;
                            break;
                        case "1":   // x1
                            btnV1.BackColor = Color.LightGreen;
                            settings.maVoltSel = 0x09;
                            break;
                        case "2":   // x10
                            btnV10.BackColor = Color.LightGreen;
                            settings.maVoltSel = 0x0A;
                            break;
                        case "3":   // x100
                            btnV100.BackColor = Color.LightGreen;
                            settings.maVoltSel = 0x0B;
                            break;
                        case "4":   // x1000
                            btnV1000.BackColor = Color.LightGreen;
                            settings.maVoltSel = 0x0C;
                            break;
                    }
                    break;
            }

        }




        private void MA_I_ClearLeds()
        {
            btnI01.UseVisualStyleBackColor = true;
            btnI1.UseVisualStyleBackColor = true;
            btnI10.UseVisualStyleBackColor = true;
            btnIAuto.UseVisualStyleBackColor = true;
        }
        private void MA_V_ClearLeds()
        {
            btnV01.UseVisualStyleBackColor = true;
            btnV1.UseVisualStyleBackColor = true;
            btnV10.UseVisualStyleBackColor = true;
            btnV100.UseVisualStyleBackColor = true;
            btnV1000.UseVisualStyleBackColor = true;
            btnVAuto.UseVisualStyleBackColor = true;
        }


        private void btnI01_Click(object sender, EventArgs e)
        {
            _com.SendCommand(CommandCode.MA_SELCURR, "0");
//            MA_I_ClearLeds();
//            btnI01.BackColor = Color.LightGreen;
        }

        private void btnI1_Click(object sender, EventArgs e)
        {
            _com.SendCommand(CommandCode.MA_SELCURR, "1");
//            MA_I_ClearLeds();
//            btnI1.BackColor = Color.LightGreen;
        }

        private void btnI10_Click(object sender, EventArgs e)
        {
            _com.SendCommand(CommandCode.MA_SELCURR, "2");

//            MA_I_ClearLeds();
//            btnI10.BackColor = Color.LightGreen;
        }

        private void btnV01_Click(object sender, EventArgs e)
        {
            _com.SendCommand(CommandCode.MA_SELVOLT, "0");
//            MA_V_ClearLeds();
//            btnV01.BackColor = Color.LightGreen;
        }

        private void btnV1_Click(object sender, EventArgs e)
        {
            _com.SendCommand(CommandCode.MA_SELVOLT, "1");
//            MA_V_ClearLeds();
//            btnV1.BackColor = Color.LightGreen;
        }

        private void btnV10_Click(object sender, EventArgs e)
        {
            _com.SendCommand(CommandCode.MA_SELVOLT, "2");
//            MA_V_ClearLeds();
//            btnV10.BackColor = Color.LightGreen;
        }

        private void btnV100_Click(object sender, EventArgs e)
        {
            _com.SendCommand(CommandCode.MA_SELVOLT, "3");
//            MA_V_ClearLeds();
//            btnV100.BackColor = Color.LightGreen;
        }

        private void btnV1000_Click(object sender, EventArgs e)
        {
            _com.SendCommand(CommandCode.MA_SELVOLT, "4");
//            MA_V_ClearLeds();
//            btnV1000.BackColor = Color.LightGreen;
        }

        private void btnIAuto_Click(object sender, EventArgs e)
        {
            _com.SendCommand(CommandCode.MA_SELCURR, ((settings.maCurrSel & 0x08) > 0) ? (settings.maCurrSel & 0x07).ToString() : "255");
            /*
            if ((settings.maCurrSel & 0x08) > 0)
            {
                _com.SendCommand(CommandCode.MA_SELCURR, (settings.maCurrSel & 0x07).ToString());
            }
            else
            {
                _com.SendCommand(CommandCode.MA_SELCURR, "255");
            }
            */
            /*
            if (settings.maCurrSel == 0xFF)
            {
                settings.maCurrSel = 0;
                btnIAuto.UseVisualStyleBackColor = true;
            }
            else
            {
                settings.maCurrSel = 0xFF;
                btnIAuto.BackColor = Color.LightGreen;
            }
            */
        }

        private void btnVAuto_Click(object sender, EventArgs e)
        {
            _com.SendCommand(CommandCode.MA_SELVOLT, ((settings.maVoltSel & 0x08) > 0) ? (settings.maVoltSel & 0x07).ToString() : "255");

            /*
            if ((settings.maVoltSel & 0x08) > 0)
            {
                _com.SendCommand(((int)CommandCode.MA_SELVOLT).ToString("x") + "," + (settings.maVoltSel & 0x07).ToString());
            }
            else
            {
                _com.SendCommand(((int)CommandCode.MA_SELVOLT).ToString("x") + ",255");
            }
            */
/*
            if (settings.maVoltSel == 0xFF)
            {
                settings.maVoltSel = 0;
                btnVAuto.UseVisualStyleBackColor = true;
            }
            else
            {
                settings.maVoltSel = 0xFF;
                btnVAuto.BackColor = Color.LightGreen;
            }
            */
        }

        #endregion

        #region Attenuator
        private void InitAttenuator()
        {
            comboResistance.Items.Add(new PresetElement("Off", 0));
            comboResistance.Items.Add(new PresetElement("10", 1));
            comboResistance.Items.Add(new PresetElement("20", 2));
            comboResistance.Items.Add(new PresetElement("50", 3));
            comboResistance.Items.Add(new PresetElement("100", 4));
            comboResistance.Items.Add(new PresetElement("200", 5));
            comboResistance.Items.Add(new PresetElement("500", 6));
            comboResistance.Items.Add(new PresetElement("1K", 7));
            comboResistance.Items.Add(new PresetElement("2K", 8));
            comboResistance.Items.Add(new PresetElement("5K", 9));
            comboResistance.Items.Add(new PresetElement("10K", 10));
            comboResistance.Items.Add(new PresetElement("20K", 11));
            comboResistance.Items.Add(new PresetElement("50K", 12));
            comboResistance.Items.Add(new PresetElement("100K", 13));
        }

        private void comboResistance_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_com.IsOpen)
            {
                _com.SendCommand(CommandCode.ATT_SELRES, ((PresetElement)comboResistance.SelectedItem).presetIndex.ToString());
            }
        }

        public void Update_lblResistance(string[] values)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<string[]>(Update_lblResistance), new object[] { values });
                return;
            }
            lblResistance.Text = values[0].ToUpper().PadLeft(9) + (values[0].ToUpper() == "OFF" ? "" :" R");
        }


        #endregion


        #region DAQ

        public void Update_Image(Image image)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<Image>(Update_Image), new object[] { image });
                return;
            }
            if (pbxScope.Image != null)
            {
                pbxScope.Image.Dispose();
            }
            pbxScope.Image = image;
            pbxScope.Refresh();
        }

        void DAQ_Process(CTResponse response)
        {
            DAQResponse dAQResponse = new DAQResponse(response);
            Bitmap scopeImage = new Bitmap(pbxScope.Width, pbxScope.Height);
            dAQResponse.Render(ref scopeImage, Color.Red, Color.Black);
//            dAQResponse.DAQ_Test_Render(ref scopeImage, Color.Red, Color.Black, ref y);
            Update_Image((Image)scopeImage);
        }
        private Bitmap _scopeImage;
        private Graphics _scopeGraphics;
        int y = 0;
        void DAQ_Test_Render(ref Bitmap bitmap, Color color, Color background)
        {
            _scopeImage = bitmap;
            _scopeGraphics = Graphics.FromImage(_scopeImage);
            _scopeGraphics.Clear(background);
            Pen scopePen = new Pen(color, 1);
            _scopeGraphics.DrawLine(scopePen, 0, y, _scopeImage.Width, y);
            _scopeGraphics.Flush();
            y++;
        }

        private void DAQ_Tick(object info)
        {
            if (_com.IsOpen)
            {
                if (_com.LastCode != CommandCode.DAQ_GETDATA)
                {
                    _com.SendCommand(CommandCode.DAQ_GETDATA);
                }
            }
            /*
            Bitmap scopeImage = new Bitmap(pbxScope.Width, pbxScope.Height);
            DAQ_Test_Render(ref scopeImage, Color.Red, Color.Black);
            Update_Image((Image)scopeImage);
            */
        }

        #endregion



        private void ReadCTParams()
        {
            _com.SendCommand(CommandCode.WFG_GETFREQ);
            _com.SendCommand(CommandCode.PA_GETVPP);
            _com.SendCommand(CommandCode.ATT_GETRES);
            _com.SendCommand(CommandCode.MA_GETRANGES);
            _com.SendCommand(CommandCode.ATT_GETDUT);
// Temporary
            _com.SendCommand(CommandCode.DAQ_GETDATA);
        }



        private void cOMSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(dlgCom.IsDisposed)
            {
                dlgCom = new ComDialog(_com);
            }
            dlgCom.cbxControllerPort.Items.Clear();
            foreach (COMPortInfo comPort in COMPortInfo.GetCOMPortsInfo())
            {
                dlgCom.cbxControllerPort.Items.Add(comPort);
            }
            dlgCom.ShowDialog();
        }

        private void connectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(_com.PortName != "")
            {
                switch(connectToolStripMenuItem.Text)
                {
                    case "Connect":
                        if (!_com.IsOpen)
                        {
                            _com.Open();
                            // _com.LineReceived += SerialDataProcessor;
                            _com.ResponseReceived += SerialDataProcessor;
                            _com.StartCommandRead(4096);
                        }
                        connectToolStripMenuItem.Text = "Disconnect";
                        // Signal the device to start the data flow
                        _com.SendCommand(CommandCode.USB_CDC_CONNECT);
                        ReadCTParams();
                        break;
                    case "Disconnect":
                        if (_com.IsOpen)
                        {
                            // signal the device to stop the data flow
                            _com.SendCommand(CommandCode.USB_CDC_DISCONNECT);
                            _com.StopCommandRead();
                            // _com.LineReceived -= SerialDataProcessor;
                            _com.ResponseReceived -= SerialDataProcessor;
                            _com.Close();
                        }
                        connectToolStripMenuItem.Text = "Connect";
                        break;
                    default:
                        break;
                }
            }
        }

        private void calibrationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dlgCal.IsDisposed)
            {
                dlgCal = new CalDialog(_com);
            }
            dlgCal.Show();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void serialLogMenuItem_Click(object sender, EventArgs e)
        {
            if (dlgSerialData.IsDisposed)
            {
                dlgSerialData = new SerialDataDialog(_com);
            }
            dlgSerialData.Show();
        }

        private void btnDUTA_Click(object sender, EventArgs e)
        {
            if (_com.IsOpen)
            {
                _com.SendCommand(CommandCode.ATT_SELDUT, new string[] { ((int)ATT_DUT.A).ToString(), "100" });
            }

        }

        private void btnDUTB_Click(object sender, EventArgs e)
        {
            if (_com.IsOpen)
            {
                _com.SendCommand(CommandCode.ATT_SELDUT, new string[] { ((int)ATT_DUT.B).ToString(), "100" });
            }
        }

        private void btnDUTAB_Click(object sender, EventArgs e)
        {
            if (_com.IsOpen)
            {
                _com.SendCommand(CommandCode.ATT_SELDUT, new string[] { ((int)ATT_DUT.ALT).ToString(), "100" });
            }
        }

        private void btnDUTOff_Click(object sender, EventArgs e)
        {
            if (_com.IsOpen)
            {
                _com.SendCommand(CommandCode.ATT_SELDUT, new string[] { ((int)ATT_DUT.OFF).ToString(), "100" });
            }
        }

        private void Update_DutButtons(string[] DUT_Data)
        {
            DUT_ClearLeds();
            switch ((ATT_DUT)int.Parse(DUT_Data[0]))
            {
                case ATT_DUT.A:
                    btnDUTA.BackColor = Color.LightGreen;
                    break;
                case ATT_DUT.B:
                    btnDUTB.BackColor = Color.LightGreen;
                    break;
                case ATT_DUT.OFF:
                    btnDUTOff.BackColor = Color.LightGreen;
                    break;
                case ATT_DUT.ALT:
                    btnDUTAB.BackColor = Color.LightGreen;
                    break;
            }
        }

        private void DUT_ClearLeds()
        {
            btnDUTA.UseVisualStyleBackColor = true;
            btnDUTB.UseVisualStyleBackColor = true;
            btnDUTAB.UseVisualStyleBackColor = true;
            btnDUTOff.UseVisualStyleBackColor = true;
        }
    }
}
