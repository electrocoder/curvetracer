/*
 * TFT.c
 *
 *  Created on: 2018. nov. 24.
 *      Author: zoli
 */

#include <stdint.h>
#include "stm32f4xx_hal.h"
#include "TFT.h"

void TFT_Init()
{
	HAL_Delay(1000);
	TFT_PLL_Enable(0x23, 0x02);
	TFT_REG = TFT_CMD_SOFT_RESET;
	HAL_Delay(100);
	TFT_Set_Lshift(0x3FFFF);

	TFT_LCD_Mode(TFT_MODE_BITS_24, TFT_WIDTH, TFT_HEIGHT);
/*
	TFT_REG=0xBA;
	TFT_RAM=0x0F;

	TFT_REG=0xB8;
	TFT_RAM=0x07;
	TFT_RAM=0x01;
*/
	TFT_REG = TFT_CMD_SET_ADDRESS_MODE;
	TFT_RAM = TFT_AM_HORI_FLIP | TFT_AM_VERT_FLIP;

	TFT_REG = TFT_CMD_SET_PIXEL_DATA_INTERFACE;
	TFT_RAM = TFT_PDI_16_565;

	TFT_REG = TFT_CMD_SET_DISPLAY_ON;
	TFT_CLS();

	TFT_FillArea(50,50,100,100,TFT_Color(0x1F,0,0));
	TFT_FillArea(100,100,150,150,TFT_Color(0,0x1F,0));
	TFT_FillArea(150,150,200,200,TFT_Color(0,0,0x1F));
	/*
	TFT_FillArea(200,200,250,250,0x0008);
	TFT_FillArea(250,250,300,300,0x0010);
	TFT_FillArea(300,300,350,350,0x0020);
	TFT_FillArea(350,350,400,400,0x0040);
	TFT_FillArea(400,400,450,450,0x0080);

	TFT_FillArea(150,50,200,100,0x0100);
	TFT_FillArea(200,100,250,150,0x0200);
	TFT_FillArea(250,150,300,200,0x0400);
	TFT_FillArea(300,200,350,250,0x0800);
	TFT_FillArea(350,250,400,300,0x1000);
	TFT_FillArea(400,300,450,350,0x2000);
	TFT_FillArea(450,350,500,400,0x4000);
	TFT_FillArea(500,400,550,450,0x8000);
	*/
	/*
	for(uint16_t i = 0; i < 50; i++)
	{
		TFT_SetPixel(i,50,0xFFFF);
	}
	*/
}

void TFT_SetArea(uint32_t x1,uint32_t y1, uint32_t x2, uint32_t y2)
{
	TFT_REG = TFT_CMD_SET_COLUMN_ADDRESS;
	TFT_RAM = x1 >> 8;
	TFT_RAM = x1 & 0x00FF;
	TFT_RAM = x2 >> 8;
	TFT_RAM = x2 & 0x00FF;
	TFT_REG = TFT_CMD_SET_PAGE_ADDRESS;
	TFT_RAM = y1 >> 8;
	TFT_RAM = y1 & 0x00FF;
	TFT_RAM = y2 >> 8;
	TFT_RAM = y2 & 0x00FF;
}

void TFT_LCD_Mode(uint16_t dw,uint16_t width, uint16_t height)
{
	TFT_REG = TFT_CMD_SET_LCD_MODE;
	TFT_RAM = dw;
	TFT_RAM = 0x00;	// TFT Mode
	TFT_RAM = (width-1) >> 8;
	TFT_RAM = (width-1) & 0x00FF;
	TFT_RAM = (height-1) >> 8;
	TFT_RAM = (height-1) & 0x00FF;
	TFT_RAM = 0; // RGB sequence

	TFT_REG = TFT_CMD_SET_HORI_PERIOD;
	TFT_RAM = (width + 128) >> 8;		// HT_H
	TFT_RAM = (width + 128) & 0x00FF;	// HT_L
	TFT_RAM = 0;						// HPS_H
	TFT_RAM = 0x2E;						// HPS_L
	TFT_RAM = 0x30;						// HPW
	TFT_RAM = 0;						// LPS_H
	TFT_RAM = 0x0F;						// LPS_L
	TFT_RAM = 0;						// LPSPP

	TFT_REG = TFT_CMD_SET_VERT_PERIOD;
	TFT_RAM = (height + 45) >> 8;		// VT_H
	TFT_RAM = (height + 45) & 0x00FF;	// VT_L
	TFT_RAM = 0;						// VPS_H
	TFT_RAM = 0x10;						// VPS_L
	TFT_RAM = 0x10;						// VPW
	TFT_RAM = 0;						// FPS_H
	TFT_RAM = 0x08;						// FPS_L

}

void TFT_Set_Lshift(uint32_t value)
{
	TFT_REG = TFT_CMD_SET_LSHIFT_FREQ;
	TFT_RAM = (value >> 16) & 0x0000000F;
	TFT_RAM = (value >> 8)  & 0x000000FF;
	TFT_RAM = value & 0x000000FF;
}

uint16_t TFT_Color(uint16_t r, uint16_t g, uint16_t b)
{
	return (r << 11) | ((g << 5) & 0x07E0) | (b & 0x001F);
}

void TFT_FillArea(uint32_t x1,uint32_t y1, uint32_t x2, uint32_t y2, uint16_t color)
{
	TFT_SetArea(x1,y1,x2,y2);
	TFT_REG = TFT_CMD_WRITE_MEMORY_START;
	for(uint32_t i = 0; i < (x2-x1+1)*(y2-y1+1); i++)
	{
		TFT_RAM = color;
	}
}

void TFT_CLS()
{
	TFT_FillArea(0,0,TFT_WIDTH-1,TFT_HEIGHT-1,0);
}

void TFT_SetPixel(uint16_t x, uint16_t y, uint16_t color)
{
	TFT_SetArea(x,y,x,y);
	TFT_REG = TFT_CMD_WRITE_MEMORY_START;
	TFT_RAM = color;
}

void TFT_PLL_Enable(uint8_t multiplier, uint8_t divider)
{
	TFT_REG = TFT_CMD_SET_PLL_MN;
	TFT_RAM = multiplier;
	TFT_RAM = divider;
	TFT_RAM = 0x04;

	TFT_REG = TFT_CMD_SET_PLL;
	TFT_RAM = TFT_PLL_ENABLE;
	HAL_Delay(10);
	TFT_REG = TFT_CMD_SET_PLL;
	TFT_RAM = TFT_PLL_LOCK | TFT_PLL_ENABLE;
	HAL_Delay(10);
}

