/*
 * measureamp.c
 *
 *  Created on: 2018. aug. 25.
 *      Author: zoli
 */

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include "measureamp.h"
#include "CurveTracer.h"
#include "MCP230xx.h"
#include "command.h"
// #include "usb_fifo.h"

uint8_t ma_curr_arr[MA_CURRENT_NUMRANGES] = {MA_CURRENT_x01, MA_CURRENT_x1, MA_CURRENT_x10};
uint8_t ma_volt_arr[MA_VOLTAGE_NUMRANGES] = {MA_VOLTAGE_x01, MA_VOLTAGE_x1, MA_VOLTAGE_x10, MA_VOLTAGE_x100, MA_VOLTAGE_x1000};
uint8_t MA_FLAGS;

// Send changes to the subscribers
// Actually just to the USB, must chnaged later for the central change/data notification system
void MA_Send_Response()
{
	/*
	char buff[80];
	uint16_t len;
	len = sprintf(buff, "%c,%u,%u,%u,%u\r",CMD_SEND_MA_PARAMS,CT_NV_Settings.maCurrSel,CT_V_Settings.maCurrRange,CT_NV_Settings.maVoltSel,CT_V_Settings.maVoltRange);
	USB_CDC_TX_write((uint8_t*)buff, len);
	*/
	CMD_Response(CMD_SEND_MA_PARAMS_T, CMD_SEND_MA_PARAMS,CT_NV_Settings.maCurrSel,CT_V_Settings.maCurrRange,CT_NV_Settings.maVoltSel,CT_V_Settings.maVoltRange);
}

void MA_Send_I2C()
{
	// Send to the device
	MCP23008_Write(I2C_ADDR_MEASUREAMP, ma_curr_arr[CT_V_Settings.maCurrRange] | ma_volt_arr[CT_V_Settings.maVoltRange]);
	MA_Send_Response();
}


///
/// MA_Range_Switcher - called by the ADC, for automatic range switching
void MA_Range_Switcher()
{
	bool IsChanged = false;
	// Automatic current range selection is active
	if(CT_NV_Settings.maCurrSel == MA_SEL_CURR_AUTO)
	{
		// Undercurrent, step up the range
		if(MA_FLAGS & MA_FLAG_UC)
		{
			if(CT_V_Settings.maCurrRange < (MA_CURRENT_NUMRANGES - 1))
			{
				CT_V_Settings.maCurrRange++;
				IsChanged = true;
			}
		}
		// Overcurrent, step down the range
		if(MA_FLAGS & MA_FLAG_OC)
		{
			if(CT_V_Settings.maCurrRange > 0)
			{
				CT_V_Settings.maCurrRange--;
				IsChanged = true;
			}
		}
	}
	// Automaic voltage range selection is active
	if(CT_NV_Settings.maVoltSel == MA_SEL_VOLT_AUTO)
	{
		// Undervoltage, step up the range
		if(MA_FLAGS & MA_FLAG_UV)
		{
			if(CT_V_Settings.maVoltRange < (MA_VOLTAGE_NUMRANGES - 1))
			{
				CT_V_Settings.maVoltRange++;
				IsChanged = true;
			}
		}
		// Overvoltage, step down the range
		if(MA_FLAGS & MA_FLAG_OV)
		{
			if(CT_V_Settings.maVoltRange > 0)
			{
				CT_V_Settings.maVoltRange--;
				IsChanged = true;
			}
		}
	}
	// Clear flags
	MA_FLAGS = 0;
	if(IsChanged)
	{
		MA_Send_I2C();
	}
}

void MA_SelCurr(uint8_t index)
{
	if(CT_NV_Settings.maCurrSel != index)
	{
		CT_NV_Settings.maCurrSel = index;
		if(index != 255)
		{
			CT_V_Settings.maCurrRange = index;
			MA_Send_I2C();
		}
		else
		{
			MA_Send_Response();
		}
	}
}

void MA_CMD_SelCurr(uint8_t source, void* args[], uint8_t len)
{
	MA_SelCurr(*((uint8_t*)args[0]));
}

uint8_t MA_CMD_SelCurr_Params[1] = {CMD_TYPE_UINT8};

CMD_CommandTypeDef MA_CMD_SelCurr_Descriptor =
{
	.CommandCode = CMD_CODE_MA_SELCURR,
	.NumParams = 1,
	.CommandProcessor = &MA_CMD_SelCurr,
	.ParamArr = MA_CMD_SelCurr_Params
};

void MA_SelVolt(uint8_t index)
{
	if(CT_NV_Settings.maVoltSel != index)
	{
		CT_NV_Settings.maVoltSel = index;
		if(index != 255)
		{
			CT_V_Settings.maVoltRange = index;
			MA_Send_I2C();
		}
		else
		{
			MA_Send_Response();
		}
	}
}

void MA_CMD_SelVolt(uint8_t source, void* args[], uint8_t len)
{
	MA_SelVolt(*((uint8_t*)args[0]));
}

uint8_t MA_CMD_SelVolt_Params[1] = {CMD_TYPE_UINT8};

CMD_CommandTypeDef MA_CMD_SelVolt_Descriptor =
{
	.CommandCode = CMD_CODE_MA_SELVOLT,
	.NumParams = 1,
	.CommandProcessor = &MA_CMD_SelVolt,
	.ParamArr = MA_CMD_SelVolt_Params
};

void MA_CMD_GetRanges(uint8_t source, void* args[], uint8_t len)
{
	MA_Send_Response();
}

CMD_CommandTypeDef MA_CMD_GetRanges_Descriptor =
{
	.CommandCode = CMD_CODE_MA_GETRANGES,
	.NumParams = 1,
	.CommandProcessor = &MA_CMD_GetRanges,
	.ParamArr = NULL
};


void MA_CMD_Register()
{
	CMD_RegisterCommand(MA_CMD_SelCurr_Descriptor);
	CMD_RegisterCommand(MA_CMD_SelVolt_Descriptor);
	CMD_RegisterCommand(MA_CMD_GetRanges_Descriptor);
}


void MA_Init()
{
	MA_CMD_Register();
	MCP23008_Init(I2C_ADDR_MEASUREAMP);
	MA_SelCurr(MA_SEL_CURR_x1);
	MA_SelVolt(MA_SEL_VOLT_x1);
}
