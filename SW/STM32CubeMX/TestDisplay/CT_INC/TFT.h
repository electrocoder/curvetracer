/*
 * TFT.h
 *
 *  Created on: 2018. nov. 24.
 *      Author: zoli
 */

#ifndef TFT_H_
#define TFT_H_

#include <stdint.h>

#define TFT_WIDTH	800
#define TFT_HEIGHT	480


// SSD1963 Register definitions
#define TFT_REG      (*((volatile uint16_t *) 0x60000000))
#define TFT_RAM      (*((volatile uint16_t *) 0x60020000))

// SSD1963 Command definitions
#define TFT_CMD_NOP							0x00
#define TFT_CMD_SOFT_RESET					0x01
#define TFT_CMD_GET_POWER_MODE				0x0A
#define TFT_CMD_GET_ADDRESS_MODE			0x0B
#define TFT_CMD_GET_DISPLAY_MODE			0x0D
#define TFT_CMD_GET_TEAR_EFFECT_STATUS		0x0E
#define TFT_CMD_ENTER_SLEEP_MODE			0x10
#define TFT_CMD_EXIT_SLEEP_MODE				0x11
#define TFT_CMD_ENTER_PARTIAL_MODE			0x12
#define TFT_CMD_ENTER_NORMAL_MODE			0x13
#define TFT_CMD_EXIT_INVERT_MODE			0x20
#define TFT_CMD_ENTER_INVERT_MODE			0x21
#define TFT_CMD_SET_GAMMA_CURVE				0x26
#define TFT_CMD_SET_DISPLAY_OFF				0x28
#define TFT_CMD_SET_DISPLAY_ON				0x29
#define TFT_CMD_SET_COLUMN_ADDRESS			0x2A
#define TFT_CMD_SET_PAGE_ADDRESS			0x2B
#define TFT_CMD_WRITE_MEMORY_START			0x2C
#define TFT_CMD_READ_MEMORY_START			0x2E
#define TFT_CMD_SET_PARTIAL_AREA			0x30
#define TFT_CMD_SET_SCROLL_AREA				0x33
#define TFT_CMD_SET_TEAR_OFF				0x34
#define TFT_CMD_SET_TEAR_ON					0x35
#define TFT_CMD_SET_ADDRESS_MODE			0x36
#define TFT_CMD_SET_SCROLL_START			0x37
#define TFT_CMD_EXIT_IDLE_MODE				0x38
#define TFT_CMD_ENTER_IDLE_MODE				0x39
#define TFT_CMD_WRITE_MEMORY_CONTINUE		0x3C
#define TFT_CMD_READ_MEMORY_CONTINUE		0x3E
#define TFT_CMD_SET_TEAR_SCANLINE			0x44
#define TFT_CMD_GET_SCANLINE				0x45
#define TFT_CMD_READ_DDB					0xA1
#define TFT_CMD_SET_LCD_MODE				0xB0
#define TFT_CMD_GET_LCD_MODE				0xB1
#define TFT_CMD_SET_HORI_PERIOD				0xB4
#define TFT_CMD_GET_HORI_PERIOD				0xB5
#define TFT_CMD_SET_VERT_PERIOD				0xB6
#define TFT_CMD_GET_VERT_PERIOD				0xB7
#define TFT_CMD_SET_GPIO_CONF				0xB8
#define TFT_CMD_GET_GPIO_CONF				0xB9
#define TFT_CMD_SET_GPIO_VALUE				0xBA
#define TFT_CMD_GET_GPIO_STATUS				0xBB
#define TFT_CMD_SET_POST_PROC				0xBC
#define TFT_CMD_GET_POST_PROC				0xBD
#define TFT_CMD_SET_PWM_CONF				0xBE
#define TFT_CMD_GET_PWM_CONF				0xBF
#define TFT_CMD_SET_LCD_GEN0				0xC0
#define TFT_CMD_GET_LCD_GEN0				0xC1
#define TFT_CMD_SET_LCD_GEN1				0xC2
#define TFT_CMD_GET_LCD_GEN1				0xC3
#define TFT_CMD_SET_LCD_GEN2				0xC4
#define TFT_CMD_GET_LCD_GEN2				0xC5
#define TFT_CMD_SET_LCD_GEN3				0xC6
#define TFT_CMD_GET_LCD_GEN3				0xC7
#define TFT_CMD_SET_GPIO0_ROP				0xC8
#define TFT_CMD_GET_GPIO0_ROP				0xC9
#define TFT_CMD_SET_GPIO1_ROP				0xCA
#define TFT_CMD_GET_GPIO1_ROP				0xCB
#define TFT_CMD_SET_GPIO2_ROP				0xCC
#define TFT_CMD_GET_GPIO2_ROP				0xCD
#define TFT_CMD_SET_GPIO3_ROP				0xCE
#define TFT_CMD_GET_GPIO3_ROP				0xCF
#define TFT_CMD_SET_DBC_CONF				0xD0
#define TFT_CMD_GET_DBC_CONF				0xD1
#define TFT_CMD_SET_DBC_TH					0xD4
#define TFT_CMD_GET_DBC_TH					0xD5
#define TFT_CMD_SET_PLL						0xE0
#define TFT_CMD_SET_PLL_MN					0xE2
#define TFT_CMD_GET_PLL_MN					0xE3
#define TFT_CMD_GET_PLL_STATUS				0xE4
#define TFT_CMD_SET_DEEP_SLEEP				0xE5
#define TFT_CMD_SET_LSHIFT_FREQ				0xE6
#define TFT_CMD_GET_LSHIFT_FREQ				0xE7
#define TFT_CMD_SET_PIXEL_DATA_INTERFACE	0xF0
#define TFT_CMD_GET_PIXEL_DATA_INTERFACE	0xF1

// Pixel data interface width
#define TFT_PDI_8		0x00
#define TFT_PDI_12		0x01
#define TFT_PDI_16		0x02
#define TFT_PDI_16_565	0x03
#define TFT_PDI_18		0x04
#define TFT_PDI_24		0x05
#define TFT_PDI_9		0x06

// Address Mode parameters
#define TFT_AM_PAO_REVERSE		0x80
#define TFT_AM_CAO_REVERSE		0x40
#define TFT_AM_PCAO_REVERSE		0x20
#define TFT_AM_LAO_REVERSE		0x10
#define TFT_AM_COLOR_ORDER_BGR	0x08
#define TFT_AM_VLAO_REVERSE		0x04
#define TFT_AM_HORI_FLIP		0x02
#define TFT_AM_VERT_FLIP		0x01

// LCD Mode parameters
#define TFT_MODE_BITS_18		0x00
#define TFT_MODE_BITS_24		0x20

// PLL
#define TFT_PLL_LOCK	0x02
#define TFT_PLL_ENABLE	0x01


// Functions
void TFT_Init();
void TFT_CLS();
void TFT_LCD_Mode(uint16_t dw,uint16_t width, uint16_t height);
void TFT_SetPixel(uint16_t x, uint16_t y, uint16_t color);
void TFT_Set_Lshift(uint32_t value);
void TFT_PLL_Enable(uint8_t multiplier, uint8_t divider);
void TFT_FillArea(uint32_t x1,uint32_t y1, uint32_t x2, uint32_t y2, uint16_t color);
uint16_t TFT_Color(uint16_t r, uint16_t g, uint16_t b);


#endif /* TFT_H_ */
