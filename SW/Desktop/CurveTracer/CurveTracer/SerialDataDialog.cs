﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CurveTracer
{
    public partial class SerialDataDialog : Form
    {
        SerialCom _com;
        public SerialDataDialog(SerialCom com)
        {
            InitializeComponent();
            this.tbxSerialLog.Text = "";
            this._com = com;
        }

        private void _com_LineSent(string line)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<string>(_com_LineSent), new object[] { line });
                return;
            }
            this.tbxSerialLog.Text += "< " + line + "\r\n";
        }
        /// <summary>
        /// Logging function. The received data is added to the Text property of the dialog
        /// </summary>
        /// <param name="line"></param>
        private void _com_LineReceived(string line)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<string>(_com_LineReceived), new object[] { line });
                return;
            }
            // Binary data is truncated from the logging. The binary data starts with !
            int binaryPos = line.IndexOf('!');
            this.tbxSerialLog.Text += "> " + ((binaryPos > -1) ? line.Substring(0, binaryPos) : line) + "\r\n";
        }

        private void _com_LineReceived(CTResponse command)
        {
            _com_LineReceived(command.Code + string.Join(",",command.Params));
        }

        ~SerialDataDialog()
        {
            this.tbxSerialLog.Text = "";
            // this._com.LineReceived -= _com_LineReceived;
            this._com.ResponseReceived -= _com_LineReceived;
            this._com.LineSent -= _com_LineSent;
        }

        private void tbxSerialLog_TextChanged(object sender, EventArgs e)
        {
            tbxSerialLog.SelectionStart = tbxSerialLog.TextLength;
            tbxSerialLog.ScrollToCaret();
        }

        private void SerialDataDialog_Shown(object sender, EventArgs e)
        {
            // this._com.LineReceived += _com_LineReceived;
            this._com.ResponseReceived -= _com_LineReceived;
            this._com.LineSent += _com_LineSent;
        }
    }
}
