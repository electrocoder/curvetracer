﻿namespace CurveTracer
{
    partial class CurveTracer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnFreqDown = new System.Windows.Forms.Button();
            this.btnFreqUp = new System.Windows.Forms.Button();
            this.comboFreq = new System.Windows.Forms.ComboBox();
            this.cbxFreqPreset = new System.Windows.Forms.CheckBox();
            this.lblFreq = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnVAuto = new System.Windows.Forms.Button();
            this.btnV1000 = new System.Windows.Forms.Button();
            this.btnV100 = new System.Windows.Forms.Button();
            this.btnV10 = new System.Windows.Forms.Button();
            this.btnV1 = new System.Windows.Forms.Button();
            this.btnV01 = new System.Windows.Forms.Button();
            this.btnIAuto = new System.Windows.Forms.Button();
            this.btnI10 = new System.Windows.Forms.Button();
            this.btnI1 = new System.Windows.Forms.Button();
            this.btnI01 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnScan = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.comboVolt = new System.Windows.Forms.ComboBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.lblTestVoltage = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.comboResistance = new System.Windows.Forms.ComboBox();
            this.lblResistance = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.btnDUTOff = new System.Windows.Forms.Button();
            this.btnDUTAB = new System.Windows.Forms.Button();
            this.btnDUTB = new System.Windows.Forms.Button();
            this.btnDUTA = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.controlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cOMSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.connectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.serialLogMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.advancedSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.calibrationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.pbxScope = new System.Windows.Forms.PictureBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxScope)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnFreqDown);
            this.groupBox1.Controls.Add(this.btnFreqUp);
            this.groupBox1.Controls.Add(this.comboFreq);
            this.groupBox1.Controls.Add(this.cbxFreqPreset);
            this.groupBox1.Controls.Add(this.lblFreq);
            this.groupBox1.Location = new System.Drawing.Point(818, 39);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(228, 154);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Frequency";
            // 
            // btnFreqDown
            // 
            this.btnFreqDown.Location = new System.Drawing.Point(160, 92);
            this.btnFreqDown.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnFreqDown.Name = "btnFreqDown";
            this.btnFreqDown.Size = new System.Drawing.Size(28, 23);
            this.btnFreqDown.TabIndex = 4;
            this.btnFreqDown.Text = "▼";
            this.btnFreqDown.UseVisualStyleBackColor = true;
            this.btnFreqDown.Click += new System.EventHandler(this.btnFreqDown_Click);
            // 
            // btnFreqUp
            // 
            this.btnFreqUp.Location = new System.Drawing.Point(160, 71);
            this.btnFreqUp.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnFreqUp.Name = "btnFreqUp";
            this.btnFreqUp.Size = new System.Drawing.Size(28, 23);
            this.btnFreqUp.TabIndex = 3;
            this.btnFreqUp.Text = "▲";
            this.btnFreqUp.UseVisualStyleBackColor = true;
            this.btnFreqUp.Click += new System.EventHandler(this.btnFreqUp_Click);
            // 
            // comboFreq
            // 
            this.comboFreq.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboFreq.FormattingEnabled = true;
            this.comboFreq.Location = new System.Drawing.Point(11, 80);
            this.comboFreq.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboFreq.Name = "comboFreq";
            this.comboFreq.Size = new System.Drawing.Size(143, 24);
            this.comboFreq.TabIndex = 2;
            this.comboFreq.SelectedIndexChanged += new System.EventHandler(this.comboFreq_SelectedIndexChanged);
            this.comboFreq.DropDownStyleChanged += new System.EventHandler(this.comboFreq_DropDownStyleChanged);
            this.comboFreq.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboFreq_KeyPress);
            // 
            // cbxFreqPreset
            // 
            this.cbxFreqPreset.AutoSize = true;
            this.cbxFreqPreset.Checked = true;
            this.cbxFreqPreset.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbxFreqPreset.Location = new System.Drawing.Point(12, 52);
            this.cbxFreqPreset.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbxFreqPreset.Name = "cbxFreqPreset";
            this.cbxFreqPreset.Size = new System.Drawing.Size(71, 21);
            this.cbxFreqPreset.TabIndex = 1;
            this.cbxFreqPreset.Text = "Preset";
            this.cbxFreqPreset.UseVisualStyleBackColor = true;
            this.cbxFreqPreset.CheckedChanged += new System.EventHandler(this.cbxFreqPreset_CheckedChanged);
            // 
            // lblFreq
            // 
            this.lblFreq.AutoSize = true;
            this.lblFreq.Font = new System.Drawing.Font("Courier New", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblFreq.Location = new System.Drawing.Point(5, 18);
            this.lblFreq.Name = "lblFreq";
            this.lblFreq.Size = new System.Drawing.Size(201, 31);
            this.lblFreq.TabIndex = 0;
            this.lblFreq.Text = "0000.000 Hz";
            this.lblFreq.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.btnVAuto);
            this.groupBox2.Controls.Add(this.btnV1000);
            this.groupBox2.Controls.Add(this.btnV100);
            this.groupBox2.Controls.Add(this.btnV10);
            this.groupBox2.Controls.Add(this.btnV1);
            this.groupBox2.Controls.Add(this.btnV01);
            this.groupBox2.Controls.Add(this.btnIAuto);
            this.groupBox2.Controls.Add(this.btnI10);
            this.groupBox2.Controls.Add(this.btnI1);
            this.groupBox2.Controls.Add(this.btnI01);
            this.groupBox2.Location = new System.Drawing.Point(818, 313);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(484, 106);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Measure";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Courier New", 16.2F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(8, 55);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 31);
            this.label4.TabIndex = 17;
            this.label4.Text = "V";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Courier New", 16.2F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(8, 20);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 31);
            this.label3.TabIndex = 16;
            this.label3.Text = "I";
            // 
            // btnVAuto
            // 
            this.btnVAuto.Location = new System.Drawing.Point(405, 55);
            this.btnVAuto.Margin = new System.Windows.Forms.Padding(4);
            this.btnVAuto.Name = "btnVAuto";
            this.btnVAuto.Size = new System.Drawing.Size(60, 28);
            this.btnVAuto.TabIndex = 15;
            this.btnVAuto.Text = "AUTO";
            this.btnVAuto.UseVisualStyleBackColor = true;
            this.btnVAuto.Click += new System.EventHandler(this.btnVAuto_Click);
            // 
            // btnV1000
            // 
            this.btnV1000.Location = new System.Drawing.Point(337, 55);
            this.btnV1000.Margin = new System.Windows.Forms.Padding(4);
            this.btnV1000.Name = "btnV1000";
            this.btnV1000.Size = new System.Drawing.Size(60, 28);
            this.btnV1000.TabIndex = 14;
            this.btnV1000.Text = "x1000";
            this.btnV1000.UseVisualStyleBackColor = true;
            this.btnV1000.Click += new System.EventHandler(this.btnV1000_Click);
            // 
            // btnV100
            // 
            this.btnV100.Location = new System.Drawing.Point(269, 55);
            this.btnV100.Margin = new System.Windows.Forms.Padding(4);
            this.btnV100.Name = "btnV100";
            this.btnV100.Size = new System.Drawing.Size(60, 28);
            this.btnV100.TabIndex = 13;
            this.btnV100.Text = "x100";
            this.btnV100.UseVisualStyleBackColor = true;
            this.btnV100.Click += new System.EventHandler(this.btnV100_Click);
            // 
            // btnV10
            // 
            this.btnV10.Location = new System.Drawing.Point(201, 55);
            this.btnV10.Margin = new System.Windows.Forms.Padding(4);
            this.btnV10.Name = "btnV10";
            this.btnV10.Size = new System.Drawing.Size(60, 28);
            this.btnV10.TabIndex = 12;
            this.btnV10.Text = "x10";
            this.btnV10.UseVisualStyleBackColor = true;
            this.btnV10.Click += new System.EventHandler(this.btnV10_Click);
            // 
            // btnV1
            // 
            this.btnV1.Location = new System.Drawing.Point(133, 55);
            this.btnV1.Margin = new System.Windows.Forms.Padding(4);
            this.btnV1.Name = "btnV1";
            this.btnV1.Size = new System.Drawing.Size(60, 28);
            this.btnV1.TabIndex = 11;
            this.btnV1.Text = "x1";
            this.btnV1.UseVisualStyleBackColor = true;
            this.btnV1.Click += new System.EventHandler(this.btnV1_Click);
            // 
            // btnV01
            // 
            this.btnV01.Location = new System.Drawing.Point(65, 55);
            this.btnV01.Margin = new System.Windows.Forms.Padding(4);
            this.btnV01.Name = "btnV01";
            this.btnV01.Size = new System.Drawing.Size(60, 28);
            this.btnV01.TabIndex = 10;
            this.btnV01.Text = "x0.1";
            this.btnV01.UseVisualStyleBackColor = true;
            this.btnV01.Click += new System.EventHandler(this.btnV01_Click);
            // 
            // btnIAuto
            // 
            this.btnIAuto.Location = new System.Drawing.Point(405, 20);
            this.btnIAuto.Margin = new System.Windows.Forms.Padding(4);
            this.btnIAuto.Name = "btnIAuto";
            this.btnIAuto.Size = new System.Drawing.Size(60, 28);
            this.btnIAuto.TabIndex = 9;
            this.btnIAuto.Text = "AUTO";
            this.btnIAuto.UseVisualStyleBackColor = true;
            this.btnIAuto.Click += new System.EventHandler(this.btnIAuto_Click);
            // 
            // btnI10
            // 
            this.btnI10.Location = new System.Drawing.Point(201, 20);
            this.btnI10.Margin = new System.Windows.Forms.Padding(4);
            this.btnI10.Name = "btnI10";
            this.btnI10.Size = new System.Drawing.Size(60, 28);
            this.btnI10.TabIndex = 6;
            this.btnI10.Text = "x10";
            this.btnI10.UseVisualStyleBackColor = true;
            this.btnI10.Click += new System.EventHandler(this.btnI10_Click);
            // 
            // btnI1
            // 
            this.btnI1.Location = new System.Drawing.Point(133, 20);
            this.btnI1.Margin = new System.Windows.Forms.Padding(4);
            this.btnI1.Name = "btnI1";
            this.btnI1.Size = new System.Drawing.Size(60, 28);
            this.btnI1.TabIndex = 5;
            this.btnI1.Text = "x1";
            this.btnI1.UseVisualStyleBackColor = true;
            this.btnI1.Click += new System.EventHandler(this.btnI1_Click);
            // 
            // btnI01
            // 
            this.btnI01.BackColor = System.Drawing.SystemColors.Control;
            this.btnI01.Location = new System.Drawing.Point(65, 20);
            this.btnI01.Margin = new System.Windows.Forms.Padding(4);
            this.btnI01.Name = "btnI01";
            this.btnI01.Size = new System.Drawing.Size(60, 28);
            this.btnI01.TabIndex = 0;
            this.btnI01.Text = "x0.1";
            this.btnI01.UseVisualStyleBackColor = true;
            this.btnI01.Click += new System.EventHandler(this.btnI01_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnScan);
            this.groupBox3.Controls.Add(this.button3);
            this.groupBox3.Controls.Add(this.button2);
            this.groupBox3.Controls.Add(this.comboVolt);
            this.groupBox3.Controls.Add(this.checkBox1);
            this.groupBox3.Controls.Add(this.lblTestVoltage);
            this.groupBox3.Location = new System.Drawing.Point(1072, 41);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(228, 153);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Voltage";
            // 
            // btnScan
            // 
            this.btnScan.Location = new System.Drawing.Point(12, 111);
            this.btnScan.Margin = new System.Windows.Forms.Padding(4);
            this.btnScan.Name = "btnScan";
            this.btnScan.Size = new System.Drawing.Size(100, 28);
            this.btnScan.TabIndex = 0;
            this.btnScan.Text = "SCAN";
            this.btnScan.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(160, 91);
            this.button3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(28, 23);
            this.button3.TabIndex = 5;
            this.button3.Text = "▼";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(160, 70);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(28, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "▲";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // comboVolt
            // 
            this.comboVolt.FormattingEnabled = true;
            this.comboVolt.Location = new System.Drawing.Point(12, 79);
            this.comboVolt.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboVolt.Name = "comboVolt";
            this.comboVolt.Size = new System.Drawing.Size(143, 24);
            this.comboVolt.TabIndex = 3;
            this.comboVolt.SelectedIndexChanged += new System.EventHandler(this.comboVolt_SelectedIndexChanged);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(12, 53);
            this.checkBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(71, 21);
            this.checkBox1.TabIndex = 3;
            this.checkBox1.Text = "Preset";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // lblTestVoltage
            // 
            this.lblTestVoltage.AutoSize = true;
            this.lblTestVoltage.Font = new System.Drawing.Font("Courier New", 16.2F, System.Drawing.FontStyle.Bold);
            this.lblTestVoltage.Location = new System.Drawing.Point(69, 20);
            this.lblTestVoltage.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTestVoltage.Name = "lblTestVoltage";
            this.lblTestVoltage.Size = new System.Drawing.Size(116, 31);
            this.lblTestVoltage.TabIndex = 0;
            this.lblTestVoltage.Text = "00.0 V";
            this.lblTestVoltage.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.button4);
            this.groupBox4.Controls.Add(this.button5);
            this.groupBox4.Controls.Add(this.comboResistance);
            this.groupBox4.Controls.Add(this.lblResistance);
            this.groupBox4.Location = new System.Drawing.Point(818, 199);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox4.Size = new System.Drawing.Size(228, 105);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Resistance";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(164, 74);
            this.button4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(28, 23);
            this.button4.TabIndex = 5;
            this.button4.Text = "▼";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(164, 53);
            this.button5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(28, 23);
            this.button5.TabIndex = 4;
            this.button5.Text = "▲";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // comboResistance
            // 
            this.comboResistance.FormattingEnabled = true;
            this.comboResistance.Location = new System.Drawing.Point(15, 62);
            this.comboResistance.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboResistance.Name = "comboResistance";
            this.comboResistance.Size = new System.Drawing.Size(143, 24);
            this.comboResistance.TabIndex = 3;
            this.comboResistance.SelectedIndexChanged += new System.EventHandler(this.comboResistance_SelectedIndexChanged);
            // 
            // lblResistance
            // 
            this.lblResistance.AutoSize = true;
            this.lblResistance.Font = new System.Drawing.Font("Courier New", 16.2F, System.Drawing.FontStyle.Bold);
            this.lblResistance.Location = new System.Drawing.Point(9, 19);
            this.lblResistance.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblResistance.Name = "lblResistance";
            this.lblResistance.Size = new System.Drawing.Size(201, 31);
            this.lblResistance.TabIndex = 0;
            this.lblResistance.Text = "000000.00 R";
            this.lblResistance.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.comboBox3);
            this.groupBox5.Controls.Add(this.btnDUTOff);
            this.groupBox5.Controls.Add(this.btnDUTAB);
            this.groupBox5.Controls.Add(this.btnDUTB);
            this.groupBox5.Controls.Add(this.btnDUTA);
            this.groupBox5.Location = new System.Drawing.Point(1074, 201);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox5.Size = new System.Drawing.Size(228, 103);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "DUT";
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(12, 60);
            this.comboBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(131, 24);
            this.comboBox3.TabIndex = 5;
            // 
            // btnDUTOff
            // 
            this.btnDUTOff.Location = new System.Drawing.Point(152, 59);
            this.btnDUTOff.Margin = new System.Windows.Forms.Padding(4);
            this.btnDUTOff.Name = "btnDUTOff";
            this.btnDUTOff.Size = new System.Drawing.Size(63, 28);
            this.btnDUTOff.TabIndex = 3;
            this.btnDUTOff.Text = "OFF";
            this.btnDUTOff.UseVisualStyleBackColor = true;
            this.btnDUTOff.Click += new System.EventHandler(this.btnDUTOff_Click);
            // 
            // btnDUTAB
            // 
            this.btnDUTAB.Location = new System.Drawing.Point(152, 23);
            this.btnDUTAB.Margin = new System.Windows.Forms.Padding(4);
            this.btnDUTAB.Name = "btnDUTAB";
            this.btnDUTAB.Size = new System.Drawing.Size(63, 28);
            this.btnDUTAB.TabIndex = 2;
            this.btnDUTAB.Text = "A+B";
            this.btnDUTAB.UseVisualStyleBackColor = true;
            this.btnDUTAB.Click += new System.EventHandler(this.btnDUTAB_Click);
            // 
            // btnDUTB
            // 
            this.btnDUTB.Location = new System.Drawing.Point(81, 23);
            this.btnDUTB.Margin = new System.Windows.Forms.Padding(4);
            this.btnDUTB.Name = "btnDUTB";
            this.btnDUTB.Size = new System.Drawing.Size(63, 28);
            this.btnDUTB.TabIndex = 1;
            this.btnDUTB.Text = "B";
            this.btnDUTB.UseVisualStyleBackColor = true;
            this.btnDUTB.Click += new System.EventHandler(this.btnDUTB_Click);
            // 
            // btnDUTA
            // 
            this.btnDUTA.Location = new System.Drawing.Point(12, 23);
            this.btnDUTA.Margin = new System.Windows.Forms.Padding(4);
            this.btnDUTA.Name = "btnDUTA";
            this.btnDUTA.Size = new System.Drawing.Size(61, 28);
            this.btnDUTA.TabIndex = 0;
            this.btnDUTA.Text = "A";
            this.btnDUTA.UseVisualStyleBackColor = true;
            this.btnDUTA.Click += new System.EventHandler(this.btnDUTA_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.controlToolStripMenuItem,
            this.calibrationToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1309, 28);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // controlToolStripMenuItem
            // 
            this.controlToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cOMSettingsToolStripMenuItem,
            this.connectToolStripMenuItem,
            this.serialLogMenuItem,
            this.advancedSettingsToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.controlToolStripMenuItem.Name = "controlToolStripMenuItem";
            this.controlToolStripMenuItem.Size = new System.Drawing.Size(70, 24);
            this.controlToolStripMenuItem.Text = "Control";
            // 
            // cOMSettingsToolStripMenuItem
            // 
            this.cOMSettingsToolStripMenuItem.Name = "cOMSettingsToolStripMenuItem";
            this.cOMSettingsToolStripMenuItem.Size = new System.Drawing.Size(207, 26);
            this.cOMSettingsToolStripMenuItem.Text = "COM Settings";
            this.cOMSettingsToolStripMenuItem.Click += new System.EventHandler(this.cOMSettingsToolStripMenuItem_Click);
            // 
            // connectToolStripMenuItem
            // 
            this.connectToolStripMenuItem.Name = "connectToolStripMenuItem";
            this.connectToolStripMenuItem.Size = new System.Drawing.Size(207, 26);
            this.connectToolStripMenuItem.Text = "Connect";
            this.connectToolStripMenuItem.Click += new System.EventHandler(this.connectToolStripMenuItem_Click);
            // 
            // serialLogMenuItem
            // 
            this.serialLogMenuItem.Name = "serialLogMenuItem";
            this.serialLogMenuItem.Size = new System.Drawing.Size(207, 26);
            this.serialLogMenuItem.Text = "Serial Log";
            this.serialLogMenuItem.Click += new System.EventHandler(this.serialLogMenuItem_Click);
            // 
            // advancedSettingsToolStripMenuItem
            // 
            this.advancedSettingsToolStripMenuItem.Name = "advancedSettingsToolStripMenuItem";
            this.advancedSettingsToolStripMenuItem.Size = new System.Drawing.Size(207, 26);
            this.advancedSettingsToolStripMenuItem.Text = "Advanced Settings";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(207, 26);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // calibrationToolStripMenuItem
            // 
            this.calibrationToolStripMenuItem.Name = "calibrationToolStripMenuItem";
            this.calibrationToolStripMenuItem.Size = new System.Drawing.Size(94, 24);
            this.calibrationToolStripMenuItem.Text = "Calibration";
            this.calibrationToolStripMenuItem.Click += new System.EventHandler(this.calibrationToolStripMenuItem_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // pbxScope
            // 
            this.pbxScope.Location = new System.Drawing.Point(0, 31);
            this.pbxScope.Name = "pbxScope";
            this.pbxScope.Size = new System.Drawing.Size(800, 512);
            this.pbxScope.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxScope.TabIndex = 6;
            this.pbxScope.TabStop = false;
            // 
            // CurveTracer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1309, 553);
            this.Controls.Add(this.pbxScope);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "CurveTracer";
            this.Text = "CurveTracer";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxScope)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblFreq;
        private System.Windows.Forms.Button btnFreqDown;
        private System.Windows.Forms.Button btnFreqUp;
        private System.Windows.Forms.ComboBox comboFreq;
        private System.Windows.Forms.CheckBox cbxFreqPreset;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnScan;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox comboVolt;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label lblTestVoltage;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.ComboBox comboResistance;
        private System.Windows.Forms.Label lblResistance;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btnDUTOff;
        private System.Windows.Forms.Button btnDUTAB;
        private System.Windows.Forms.Button btnDUTB;
        private System.Windows.Forms.Button btnDUTA;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnVAuto;
        private System.Windows.Forms.Button btnV1000;
        private System.Windows.Forms.Button btnV100;
        private System.Windows.Forms.Button btnV10;
        private System.Windows.Forms.Button btnV1;
        private System.Windows.Forms.Button btnV01;
        private System.Windows.Forms.Button btnIAuto;
        private System.Windows.Forms.Button btnI10;
        private System.Windows.Forms.Button btnI1;
        private System.Windows.Forms.Button btnI01;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem controlToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cOMSettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem connectToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem calibrationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem serialLogMenuItem;
        private System.Windows.Forms.ToolStripMenuItem advancedSettingsToolStripMenuItem;
        private System.Windows.Forms.PictureBox pbxScope;
    }
}

