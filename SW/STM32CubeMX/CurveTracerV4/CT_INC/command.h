/*
 * command.h
 *
 *  Created on: 2018. aug. 20.
 *      Author: zoli
 */

#ifndef COMMAND_H_
#define COMMAND_H_

#include <stdint.h>

#define CMD_MAX_NUMCOMMANDS 25

#define CMD_TYPE_NONE	0
#define CMD_TYPE_UINT8	1
#define CMD_TYPE_INT8	2
#define CMD_TYPE_UINT16	3
#define CMD_TYPE_INT16	4
#define CMD_TYPE_UINT32	5
#define CMD_TYPE_INT32	6
#define CMD_TYPE_DOUBLE	7

#define CMD_COM_DEVICE_CON	0	// Device local console (keypad, rotary encoders, local display, LEDs - maybe redefined to more than one device later
#define CMD_COM_DEVICE_USB	1	// USB
#define CMD_COM_DEVICE_ETH	2	// Ethernet (probably HTTP)

#define CMD_CODE_WFG_SETPERIOD	0
#define CMD_CODE_WFG_SELFREQ	1
#define CMD_CODE_WFG_SETFREQ	2
#define CMD_CODE_WFG_GETFREQ	3
#define CMD_CODE_WFG_GENSINTBL	4
#define CMD_CODE_WFG_GENCALTBL	5

#define CMD_CODE_WFG_MODE			6
#define CMD_CODE_WFG_SETCALSWING	7
#define CMD_CODE_WFG_SETCALPOINT	8
#define CMD_CODE_WFG_GETCALDATA		9


#define CMD_CODE_PA_SETWIPER	10
#define CMD_CODE_PA_SELVPP		11
#define CMD_CODE_PA_SETVPP		12
#define CMD_CODE_PA_GETVPP		13

#define CMD_CODE_ATT_SELRES		14
#define CMD_CODE_ATT_SELDUT		15
#define CMD_CODE_ATT_GETRES		16
#define CMD_CODE_ATT_GETDUT		17
#define CMD_CODE_ATT_SETALTCYCLES	18

#define CMD_CODE_MA_SELCURR		19
#define CMD_CODE_MA_SELVOLT		20
#define CMD_CODE_MA_GETRANGES	21

#define CMD_CODE_DAQ_GETDATA	22

#define CMD_CODE_USB_CDC_CONNECT	23
#define CMD_CODE_USB_CDC_DISCONNECT	24


#define CMD_SEND_FREQ		'f'
#define CMD_SEND_FREQ_T		"%c,%.3f\r"
#define CMD_SEND_MA_PARAMS	'm'
#define CMD_SEND_MA_PARAMS_T	"%c,%u,%u,%u,%u\r"
#define CMD_SEND_PA_VPP		'v'
#define CMD_SEND_PA_VPP_T	"%c,%.1f\r"
#define CMD_SEND_CAL		'c'
#define CMD_SEND_CAL_T		"%c,%u,%u\r"
#define CMD_SEND_RES		'r'
#define CMD_SEND_RES_T		"%c,%.2f\r"
#define CMD_SEND_RES_OFF	"%c,off\r"
#define CMD_SEND_DUT		'd'
#define CMD_SEND_DUT_T		"%c,%u,%u,%u\r"
#define CMD_SEND_DATA		'a'
// Temporary - Just one ADC Channel. the serial number and the data
// #define CMD_SEND_DATA_T		"%c,%u,%u\r"
// Send this before raw data - the ! designate, that binary data coming
#define CMD_SEND_DATA_T		"%c,%u,%u,%u,%u,!"



typedef struct
{
	uint8_t CommandCode;
	uint8_t NumParams;
	// Command processor parameters:
	// uint8_t - command source (CON, USB, ETH, etc.)
	// void*[] - array of parameters
	// uint8_t - number of parameters
	void (* CommandProcessor)(uint8_t,void*[],uint8_t);
	const uint8_t * ParamArr;
	char ResponseCode;
} CMD_CommandTypeDef;

/*
typedef struct
{
	uint8_t ResponseType;
	char ResponseDigits;
	char ResponseFrac;
} CMD_ResponseType;
*/

typedef struct
{
	uint8_t DeviceCode;
	// handle the text returned by a command
	// params: response string, error code
	void (* ResponseProcessor)(char*,uint8_t);
} CMD_ResponseTypeDef;

void CMD_RegisterDevice(CMD_ResponseTypeDef device);
void CMD_RegisterCommand(CMD_CommandTypeDef command);
void CMD_Call(char* cmdstr, uint8_t source);
void CMD_Response_Empty(char* template, char command);
void CMD_Response_Raw(uint8_t* buff, uint16_t len);
void CMD_Response(char* template, ...);
void CMD_Response_LF();

#endif /* COMMAND_H_ */
