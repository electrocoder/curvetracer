/*
 * MCP230xx.c
 *
 *  Created on: 2018. aug. 25.
 *      Author: zoli
 */

#include "stm32f4xx_hal.h"
#include "i2c.h"
#include "MCP230xx.h"

void MCP23008_Write(uint16_t addr, uint8_t data)
{
	uint8_t buff[2] = {0x0A,data};
	HAL_I2C_Master_Transmit(&hi2c1, addr, buff, 2, 100);
}

void MCP23017_Write(uint16_t addr, uint8_t port, uint8_t data)
{
	uint8_t buff[2] = {0x12 | port, data};
	HAL_I2C_Master_Transmit(&hi2c1, addr, buff, 2, 100);
}

void MCP23017_WriteW(uint16_t addr, uint16_t data)
{
	uint8_t buff[3] = {0x12, (uint8_t)(data & 0x00FF), (uint8_t)(data >> 8)};
	HAL_I2C_Master_Transmit(&hi2c1, addr, buff, 3, 100);
}

void MCP23008_Init(uint16_t addr)
{
	uint8_t buff[2] = {0,0};
	HAL_I2C_Master_Transmit(&hi2c1, addr, buff, 2, 100);
}

void MCP23017_Init(uint16_t addr)
{
	uint8_t buff[3] = {0,0,0};
	HAL_I2C_Master_Transmit(&hi2c1, addr, buff, 3, 100);
}
