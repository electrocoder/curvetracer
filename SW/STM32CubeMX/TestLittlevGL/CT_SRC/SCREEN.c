/*
 * SCREEN.c
 *
 *  Created on: 2018. nov. 27.
 *      Author: zoli
 */

#include <stdint.h>
#include "lvgl.h"
#include "SSD1963_fsmc.h"
#include "XPT2046.h"
#include "tim.h"
#include "SCREEN.h"

#include "stm32f4xx_hal.h"
#include "gpio.h"

volatile uint8_t lvgl_period_tick;
volatile uint8_t pin_state;

void SCREEN_Init()
{
	lvgl_period_tick = 10;
	lv_disp_drv_t disp_drv;

	SSD1963_Init();

	lv_init();
	disp_drv.disp_flush = SSD1963_Flush;
	disp_drv.disp_fill = SSD1963_Fill;
	disp_drv.disp_map = SSD1963_Map;
	lv_disp_drv_register(&disp_drv);
/*
	lv_indev_drv_t indev_drv;                       // Descriptor of an input device driver
	lv_indev_drv_init(&indev_drv);                  // Basic initialization
	indev_drv.type = LV_INDEV_TYPE_POINTER;         // The touchpad is pointer type device
	indev_drv.read = XPT2046_Read;                  // Library ready your touchpad via this function
	lv_indev_drv_register(&indev_drv);              // Finally register the driver
*/
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if(htim->Instance == htim7.Instance)
	{
		if(pin_state == 0)
		{
			pin_state = 1;
		}
		else
		{
			pin_state = 0;
		}
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, pin_state);

		lv_tick_inc(1);
		if(lvgl_period_tick == 0)
		{
			lv_task_handler();
			lvgl_period_tick = 10;
		}
		else
		{
			lvgl_period_tick--;
		}

	}
}

void HAL_SYSTICK_Callback(void)
{
	/*
	lv_tick_inc(1);
	if(lvgl_period_tick == 0)
	{
		lv_task_handler();
		lvgl_period_tick = 10;
	}
	else
	{
		lvgl_period_tick--;
	}
	HAL_NVIC_ClearPendingIRQ(SysTick_IRQn);
	*/
}


void SCREEN_hello()
{
    /*Create a Label on the currently active screen*/
    lv_obj_t *label1 =  lv_label_create(lv_scr_act(), NULL);

    /*Modify the Label's text*/
    lv_label_set_text(label1, "Hello world!");

    /* Align the Label to the center
     * NULL means align on parent (which is the screen now)
     * 0, 0 at the end means an x, y offset after alignment*/
    lv_obj_align(label1, NULL, LV_ALIGN_CENTER, 0, 0);
}
