/*
 * sine.c
 *
 *  Created on: 2018. aug. 5.
 *      Author: zoli
 */

#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_tim.h"
#include "dac.h"
#include "tim.h"
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <wfg.h>
#include "command.h"
#include "CurveTracer.h"
#include "dma_sync.h"

uint16_t wfg_tbl1[WFG_MAX_SAMPLES];		// Waveform sample table 1
uint16_t wfg_tbl2[WFG_MAX_SAMPLES];		// Waveform sample table 2
uint8_t wfg_sel_tbl = 0;				// Selected waveform table
uint8_t wfg_mode = WFG_MODE_SINE;
uint8_t wfg_cal_level;

// bool wfg_reload_tbl = false;

// Preset Frequency table
// Generated on the preset frequencies:
//   20,   30,   40,   50,   60,   70,   80,   90,  100,  110,
//  120,  130,  140,  150,  160,  170,  180,  190,  200,  300,
//  400,  500,  600,  700,  800,  900, 1000, 1100, 1200, 1300,
// 1400, 1500, 1600, 1700, 1800, 1900, 2000, 3000, 4000, 5000
//
// Number of samples 400
// Timer base frequency: 84MHz
//
const uint16_t wfg_preset_period[40] =
{
		10499,   //20Hz
		6999,   //30Hz
		5249,   //40Hz
		4199,   //50Hz
		3499,   //60Hz
		2999,   //70Hz
		2624,   //80Hz
		2332,   //90Hz
		2099,   //100Hz
		1908,   //110Hz
		1749,   //120Hz
		1614,   //130Hz
		1499,   //140Hz
		1399,   //150Hz
		1312,   //160Hz
		1234,   //170Hz
		1166,   //180Hz
		1104,   //190Hz
		1049,   //200Hz
		699,   //300Hz
		524,   //400Hz
		419,   //500Hz
		349,   //600Hz
		299,   //700Hz
		262,   //800Hz
		232,   //900Hz
		209,   //1000Hz
		190,   //1100Hz
		174,   //1200Hz
		161,   //1300Hz
		149,   //1400Hz
		139,   //1500Hz
		130,   //1600Hz
		123,   //1700Hz
		116,   //1800Hz
		110,   //1900Hz
		104,   //2000Hz
		69,   //3000Hz
		52,   //4000Hz
		41   //5000Hz
};

/*
// DMA conversion complette interrupt callback
// Place to switch tables/parameters
void HAL_DAC_ConvCpltCallbackCh1(DAC_HandleTypeDef* hdac)
{
	if(wfg_reload_tbl && wfg_sel_tbl != 0)
	{
		// Configure DMA Stream data length
		hdac->DMA_Handle1->Instance->NDTR = wfg_numsamples;
		if(wfg_sel_tbl == 1)
		{
			// Configure DMA Stream source address
			hdac->DMA_Handle1->Instance->PAR = (uint32_t) wfg_tbl1;
		}
		if(wfg_sel_tbl == 2)
		{
			// Configure DMA Stream source address
			hdac->DMA_Handle1->Instance->PAR = (uint32_t) wfg_tbl2;
		}
		wfg_reload_tbl = false;
	}
}
*/

///
/// _wfg_gen_sine_tbl - Generate the sine wave table for the DAC
/// params:
///		forceload - Set true for enforce the load of the new table
///					During the init the table reload is not enforced

void _wfg_gen_sine_tbl(bool forcereload)
{
	uint16_t i;
	uint16_t * wfg_tbl;
	switch(wfg_sel_tbl)
	{
		// 0 - No table was selected
		// switch to table 1
		case 0:
			wfg_sel_tbl=1;
			wfg_tbl = (uint16_t *)wfg_tbl1;
			break;
		// 1 - first table was selected
	    // switch to table 2
		case 1:
			wfg_sel_tbl=2;
			wfg_tbl = wfg_tbl2;
			break;
	    // 2 - second table was selected
		// switch back to table 1
		case 2:
			wfg_sel_tbl=1;
			wfg_tbl = (uint16_t *)wfg_tbl1;
			break;
		default:
			wfg_sel_tbl = 0;
			// stop the generator and return
			return;
	}
	// generate new table
	for (i = 0; i < CT_NV_Settings.wfgNumSamples; i++)
	{
		wfg_tbl[i] = round((double)CT_NV_Settings.wfgCalNullpoint + ((double)CT_NV_Settings.wfgCalSwing /2) * sin(M_PI * 2 * (double)i / (double)CT_NV_Settings.wfgNumSamples));
	}
	// wait for DMA to finish the current cycle and switch over the table (interrupt handling necessary)
	// wfg_reload_tbl = forcereload;
	if(forcereload)
	{
		WFG_Stop();
		WFG_Start();
	}
}

///
/// wfg_gen_cal_tbl - Generate DC calibration signal for the DAC
/// params:
///		level - the DC level
///				values: +1 - positive maximum: +10V at digipot value 200
///						 0 - 0V
///						-1 - negative maximum: -10V at digipot value 200
void _wfg_gen_cal_tbl(int8_t level)
{
	uint16_t i;
	uint16_t * wfg_tbl;
	switch(wfg_sel_tbl)
	{
		// 0 - No table was selected
		// switch to table 1
		case 0:
			wfg_sel_tbl=1;
			wfg_tbl = (uint16_t *)wfg_tbl1;
			break;
		// 1 - first table was selected
	    // switch to table 2
		case 1:
			wfg_sel_tbl=2;
			wfg_tbl = wfg_tbl2;
			break;
	    // 2 - second table was selected
		// switch back to table 1
		case 2:
			wfg_sel_tbl=1;
			wfg_tbl = (uint16_t *)wfg_tbl1;
			break;
		default:
			wfg_sel_tbl = 0;
			// stop the generator and return
			return;
	}
	// generate new table
	for (i = 0; i < CT_NV_Settings.wfgNumSamples; i++)
	{
		wfg_tbl[i] = CT_NV_Settings.wfgCalNullpoint + (int16_t)CT_NV_Settings.wfgCalSwing/2 * level;
	}
	// wait for DMA to finish the current cycle and switch over the table (interrupt handling necessary)
	// wfg_reload_tbl = forcereload;
	WFG_Stop();
	WFG_Start();
}

///
/// wfg_gen_tbl - Generate DC cal/sine table for the DAC
/// selected on wfg_mode variable

void _wfg_gen_tbl()
{
	switch(wfg_mode)
	{
		case WFG_MODE_SINE:
			_wfg_gen_sine_tbl(true);
			break;
		case WFG_MODE_CAL:
			_wfg_gen_cal_tbl(wfg_cal_level);
			break;
	}
}



void WFG_SetNumSamples(uint16_t samples)
{
	if(CT_NV_Settings.wfgNumSamples != samples)
	{
		CT_NV_Settings.wfgNumSamples = samples;
		CT_NV_Save();
		_wfg_gen_sine_tbl(true);
	}
}

void WFG_Stop()
{
	HAL_DAC_Stop_DMA(&hdac, DAC_CHANNEL_1);
}

void WFG_Start()
{
	if(wfg_sel_tbl == 1)
	{
		HAL_DAC_Start_DMA(&hdac, DAC_CHANNEL_1, (uint32_t*)wfg_tbl1, CT_NV_Settings.wfgNumSamples, DAC_ALIGN_12B_R);
	}
	if(wfg_sel_tbl == 2)
	{
		HAL_DAC_Start_DMA(&hdac, DAC_CHANNEL_1, (uint32_t*)wfg_tbl2, CT_NV_Settings.wfgNumSamples, DAC_ALIGN_12B_R);
	}
}

// -----------------------SetTimerPeriod-------------------------------

void WFG_SetTimerPeriod(uint16_t period)
{
	CT_NV_Settings.wfgTimePeriod = period;
	CT_NV_Save();
	// __HAL_TIM_SET_AUTORELOAD(&htim6, CT_NV_Settings.wfgTimePeriod);
	__HAL_TIM_SET_AUTORELOAD(&htim2, CT_NV_Settings.wfgTimePeriod);		// Temporarly until the two timers not joined as one
	CT_V_Settings.wfgFreq = (double)WFG_BASE_FREQ / (double)CT_NV_Settings.wfgNumSamples / (double)(CT_NV_Settings.wfgTimePeriod + 1);
	CMD_Response(CMD_SEND_FREQ_T, CMD_SEND_FREQ, CT_V_Settings.wfgFreq);
}

/// Command descriptor for WFG_SetTimerPeriod

void WFG_CMD_SetTimerPeriod(uint8_t source, void* args[], uint8_t len)
{
	WFG_SetTimerPeriod(*((uint16_t*)args[0]));
}

const uint8_t WFG_CMD_SetTimerPeriod_Params[1] = {CMD_TYPE_UINT16};

const CMD_CommandTypeDef WFG_CMD_SetTimerPeriod_Descriptor =
{
	.CommandCode = CMD_CODE_WFG_SETPERIOD,
	.NumParams = 1,
	.CommandProcessor = &WFG_CMD_SetTimerPeriod,
	.ParamArr = WFG_CMD_SetTimerPeriod_Params
};

// -----------------------SelectFreq-------------------------------

void WFG_SelectFreq(uint8_t index)
{
	WFG_SetTimerPeriod(wfg_preset_period[index]);
}

/// Command descriptor for WFG_SelectFreq

void WFG_CMD_SelectFreq(uint8_t source, void* args[], uint8_t len)
{
	WFG_SelectFreq(*((uint8_t*)args[0]));
}

const uint8_t WFG_CMD_SelectFreq_Params[1] = {CMD_TYPE_UINT8};

const CMD_CommandTypeDef WFG_CMD_SelectFreq_Descriptor =
{
	.CommandCode = CMD_CODE_WFG_SELFREQ,
	.NumParams = 1,
	.CommandProcessor = &WFG_CMD_SelectFreq,
	.ParamArr = WFG_CMD_SelectFreq_Params
};

// -----------------------SetFreq-------------------------------

void WFG_SetFreq(double freq)
{
	WFG_SetTimerPeriod((uint16_t)((double)WFG_BASE_FREQ / (double)CT_NV_Settings.wfgNumSamples / freq) + 1);
}

/// Command descriptor for WFG_SetFreq

void WFG_CMD_SetFreq(uint8_t source, void* args[], uint8_t len)
{
	WFG_SetFreq(*((double*)args[0]));
}

const uint8_t WFG_CMD_SetFreq_Params[1] = {CMD_TYPE_DOUBLE};

const CMD_CommandTypeDef WFG_CMD_SetFreq_Descriptor =
{
	.CommandCode = CMD_CODE_WFG_SETFREQ,
	.NumParams = 1,
	.CommandProcessor = &WFG_CMD_SetFreq,
	.ParamArr = WFG_CMD_SetFreq_Params
};

// -----------------------GetFreq-------------------------------

void WFG_GetFreq()
{
	CMD_Response(CMD_SEND_FREQ_T, CMD_SEND_FREQ, CT_V_Settings.wfgFreq);
}

/// Command descriptor for WFG_SetFreq

void WFG_CMD_GetFreq(uint8_t source, void* args[], uint8_t len)
{
	WFG_GetFreq();
}

const CMD_CommandTypeDef WFG_CMD_GetFreq_Descriptor =
{
	.CommandCode = CMD_CODE_WFG_GETFREQ,
	.NumParams = 0,
	.CommandProcessor = &WFG_CMD_GetFreq,
	.ParamArr = NULL
};

// -----------------------Mode-------------------------------
void WFG_Mode(uint8_t mode, uint8_t level)
{
	bool configchanged = false;
	if(wfg_mode != mode)
	{
		wfg_mode = mode;
		configchanged = true;
	}
	if(wfg_cal_level != level)
	{
		wfg_cal_level = level;
		configchanged = true;
	}
	if(configchanged)
	{
		_wfg_gen_tbl();
	}
}

/// Command descriptor for WFG_Mode

void WFG_CMD_Mode(uint8_t source, void* args[], uint8_t len)
{
	WFG_Mode(*((uint8_t*)args[0]), *((uint8_t*)args[1]));
}

const uint8_t WFG_CMD_Mode_Params[2] = {CMD_TYPE_UINT8, CMD_TYPE_UINT8};

const CMD_CommandTypeDef WFG_CMD_Mode_Descriptor =
{
	.CommandCode = CMD_CODE_WFG_MODE,
	.NumParams = 2,
	.CommandProcessor = &WFG_CMD_Mode,
	.ParamArr = WFG_CMD_Mode_Params
};

// -----------------------SetCalSwing-------------------------------

void WFG_SetCalSwing(uint16_t swing)
{
	if(CT_NV_Settings.wfgCalSwing != swing)	// table regeneration is required
	{
		CT_NV_Settings.wfgCalSwing = swing;
		CT_NV_Save();
		_wfg_gen_tbl();
	}
}

/// Command descriptor for WFG_Mode

void WFG_CMD_SetCalSwing(uint8_t source, void* args[], uint8_t len)
{
	WFG_SetCalSwing(*((uint16_t*)args[0]));
}

const uint8_t WFG_CMD_SetCalSwing_Params[1] = {CMD_TYPE_UINT16};

CMD_CommandTypeDef WFG_CMD_SetCalSwing_Descriptor =
{
	.CommandCode = CMD_CODE_WFG_SETCALSWING,
	.NumParams = 1,
	.CommandProcessor = &WFG_CMD_SetCalSwing,
	.ParamArr = WFG_CMD_SetCalSwing_Params
};

// -----------------------SetCalPoint-------------------------------

void WFG_SetCalPoint(uint16_t point)
{
	if(CT_NV_Settings.wfgCalNullpoint != point)
	{
		CT_NV_Settings.wfgCalNullpoint = point;
		CT_NV_Save();
		_wfg_gen_tbl();
	}
}

/// Command descriptor for WFG_Mode

void WFG_CMD_SetCalPoint(uint8_t source, void* args[], uint8_t len)
{
	WFG_SetCalPoint(*((uint16_t*)args[0]));
}

const uint8_t WFG_CMD_SetCalPoint_Params[1] = {CMD_TYPE_UINT16};

const CMD_CommandTypeDef WFG_CMD_SetCalPoint_Descriptor =
{
	.CommandCode = CMD_CODE_WFG_SETCALPOINT,
	.NumParams = 1,
	.CommandProcessor = &WFG_CMD_SetCalPoint,
	.ParamArr = WFG_CMD_SetCalPoint_Params
};

// -----------------------GetCalData-------------------------------

void WFG_GetCalData()
{
	CMD_Response(CMD_SEND_CAL_T, CMD_SEND_CAL, CT_NV_Settings.wfgCalNullpoint , CT_NV_Settings.wfgCalSwing);
}

/// Command descriptor for WFG_GetCalData

void WFG_CMD_GetCalData(uint8_t source, void* args[], uint8_t len)
{
	WFG_GetCalData();
}

const CMD_CommandTypeDef WFG_CMD_GetCalData_Descriptor =
{
	.CommandCode = CMD_CODE_WFG_GETCALDATA,
	.NumParams = 0,
	.CommandProcessor = &WFG_CMD_GetCalData,
	.ParamArr = NULL
};

// Register commands for the command queue
void _wfg_CMD_Register()
{
	CMD_RegisterCommand(WFG_CMD_SetTimerPeriod_Descriptor);
	CMD_RegisterCommand(WFG_CMD_SelectFreq_Descriptor);
	CMD_RegisterCommand(WFG_CMD_SetFreq_Descriptor);
	CMD_RegisterCommand(WFG_CMD_GetFreq_Descriptor);
	CMD_RegisterCommand(WFG_CMD_Mode_Descriptor);
	CMD_RegisterCommand(WFG_CMD_SetCalSwing_Descriptor);
	CMD_RegisterCommand(WFG_CMD_SetCalPoint_Descriptor);
	CMD_RegisterCommand(WFG_CMD_GetCalData_Descriptor);
}

void WFG_Init()
{
	// Register callback functions for the command center
	_wfg_CMD_Register();
	// generate the initial sinewave table
	_wfg_gen_sine_tbl(false);
	// Start conversion
	// HAL_TIM_Base_Start(&htim6);
	HAL_TIM_Base_Start(&htim2);
	HAL_DAC_Start(&hdac, DAC_CHANNEL_1);
}

