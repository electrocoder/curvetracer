﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurveTracer
{
    public class CTCommand
    {
        public CTCommand() { }
        public CTCommand(CommandCode code)
        {
            Code = code;
            Params = null;
        }

        public CTCommand(CommandCode code, string[] paramarr)
        {
            Code = code;
            Params = paramarr;
        }
        public CTCommand(CommandCode code, string param)
        {
            Code = code;
            Params = new string[1] { param };
        }
        public CommandCode Code;
        public string[] Params;
        public string Serialize()
        {
            return ((int)Code).ToString("x") + ((Params != null) ?  "," + String.Join(",", Params) : "");
        }
    }
}
