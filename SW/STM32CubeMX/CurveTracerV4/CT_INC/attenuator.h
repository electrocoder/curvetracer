/*
 * attenutor.h
 *
 *  Created on: 2018. aug. 25.
 *      Author: zoli
 */

#ifndef ATTENUATOR_H_
#define ATTENUATOR_H_

#define I2C_ADDR_ATT_IOEXT 0x42

#define ATT_DUT_OFF	0
#define ATT_DUT_1	1
#define ATT_DUT_2	2
#define ATT_DUT_ALT	3	// Alternating Device Under Test

typedef struct
{
	uint16_t gpio;
	double value;
} ATT_ResTypeDef;


void ATT_DUT_Switcher();
void ATT_SetAltCycles(uint8_t cycles);
double ATT_SelRes(uint8_t index);
void ATT_SelDUT(uint8_t DUT,uint16_t alt_cycles);
void ATT_GetRes();
void ATT_GetDUT();

void ATT_Init();

#endif /* ATTENUATOR_H_ */
